

PROGRAM _INIT
//	MpTempController_0(MpLink := , Enable := , ErrorReset := , Parameters := , Update := , SetTemperature := , ActualTemperature := , Control := , Tune := , Simulate := );
	MpTempController_0.MpLink := ADR(gTempController);
	
	ControllerParametres.Tuning.SetPointHeat := 60;
	MpTempController_0.Parameters := ADR(ControllerParametres);
	
END_PROGRAM